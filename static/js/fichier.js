let ensembleNoms = new Set()
let nb =0;
function affichePastille(id, nom, numero, dimension, posX, posY, pos_pointe_x, pos_pointe_y, type_elem, init_elem, couleur_fond, couleur_police, texte, type_pose, krelief, braille, image_pose, commentaire, valider, ral, vinyle, zone, angle, poly, produit, dico_sous_p, sauvegarde_modif, validation, idLegende, precedent_type_pose,precedent_nom,precedent_numero,precedent_krelief, precedent_braille, precedent_image_pose, precedent_commentaire, precedent_vinyle,precedent_ral, precedent_feuille_poly,precedent_zone,precedent_position_x,precedent_position_y,precedent_position_pointe_x,precedent_position_pointe_y,precedent_angle, precedent_dimension){
    if(sauvegarde_modif == "True"){
        return 0;
    }
    console.log("nom precedent " + precedent_nom);
    scale = document.getElementById("img_plan").offsetWidth / 1489;
    let div = document.createElement("div"); // créé la div qui contient les infos
    let span = document.createElement("span"); // créé le span qui contient le nom de l'element
    let divp = document.createElement("div"); // créé le cercle correspondant à la légende
    let global = document.getElementById("global");
    let str = nom.split("-");
    let innerDiv = document.createElement("div");
    divp.classList.add("couleurP");
    divp.classList.add("couleurPmove");
    divp.id = "couleur" + id;
    divp.style.background = couleur_fond;
    divp.style.color = couleur_police;
    divp.style.borderRadius = "50px";
    divp.innerText = init_elem;
    div.classList.add("div_pastille");
    if(str.length>1){
        span.innerText = str[0] + "\n" + str[1];
    }
    else {
        span.innerText = str[0];
    }
    
    innerDiv.appendChild(span);
    div.appendChild(divp);
    div.appendChild(innerDiv);
    global.appendChild(div);
    if(scale<=1){
        signe = "-";
    }
    else{
        signe = "+";
    }
    let decale_largeur = div.offsetWidth - (div.offsetWidth * scale);
    let decale_hauteur = div.offsetHeight - (div.offsetHeight * scale);
    div.style.transform = "translateX( "+ signe + (decale_largeur/2) +"px)";
    div.style.transform += "translateY( "+ signe + (decale_hauteur/2) +"px)";
    scale = scale -0.25;
    div.style.transform += "scale( "+ scale +")";
    scale += 0.25
    //div.style.transform += "scale("+ scale +")";
    div.style.left = (posX*scale)+ "px";
    div.style.top = (posY*scale) + "px";
    if(valider == "False"){
        div.style.borderColor = "orange";
        nb++;
    }

    div.id = "div_"+id;
    if(validation != "True"){
        div.addEventListener("click", function (){
            pastilleToForm(id, nom, numero, dimension, posX, posY, pos_pointe_x, pos_pointe_y, type_elem, init_elem, couleur_fond, couleur_police, texte, type_pose, krelief, braille, image_pose, commentaire, ral, vinyle, zone, angle, poly, produit, dico_sous_p, valider,precedent_type_pose,precedent_nom,precedent_numero,precedent_krelief, precedent_braille,precedent_image_pose, precedent_commentaire, precedent_vinyle,precedent_ral, precedent_feuille_poly,precedent_zone,precedent_position_x,precedent_position_y,precedent_position_pointe_x,precedent_position_pointe_y,precedent_angle, precedent_dimension);
        });
        
        divp.onmouseover = dragElement(div, false);
    }
    
    // mettre le nombre de pastille dans la légende
    let divNombre = document.getElementById("divNombre" + idLegende );
    let nombre = parseInt(divNombre.innerText);
    divNombre.innerText = nombre+1;
    affichePointeur(id,posX,posY,pos_pointe_x,pos_pointe_y, angle, couleur_fond);
    pastilleAValider(nb);
   }

   function pastilleAValider(nb){
       let div = document.getElementById("a_valider");
       let ul = div.getElementsByTagName("ul")[0];
       ul.removeChild(ul.firstChild);
       let li  = document.createElement("li");
       if(nb == 0){
           li.innerText = "Rien à valider"
        }
        else {
            li.innerText = "nombre de pastille à valider : " + nb;
        }
        ul.appendChild(li);
        div.appendChild(ul);
   }

   function affichePointeur(id, posX, posY, pos_pointeX, pos_pointeY, angle, couleur){
        scale = document.getElementById("img_plan").offsetWidth / 1489;
        if(document.getElementsByTagName("svg").length ==0 ){
        draw = SVG().addTo('#div_svg');
       }
        posY = (Number(posY) + 20) * scale;
        posX1 = (Number(posX) + 25) * scale;
        posX2 = (Number(posX) + 35) * scale;
        pos_pointeX = (Number(pos_pointeX)* scale);
        pos_pointeY = (Number(pos_pointeY) * scale)
        str = posX1 +"," + posY + " " + posX2 +"," +posY + " " + pos_pointeX+","+pos_pointeY;
        let polygon = draw.polygon(str);
        polygon.addClass('pointeur_'+id);
        polygon.fill(couleur);
        let path = draw.path("M50,400 C58,385 72,385 80,400 z").fill(couleur);
        path.size(15*scale,3*scale);
        path.center(pos_pointeX,pos_pointeY);
        path.addClass("path_"+id);
        path.rotate(angle);
        path.addClass("move")
        path.on("mouseover", function(){
            dragElement(path,true);
        })
        
    }

    function modifPointeur(id_pointeur, posX = null, posY = null, pos_pointeX = null, pos_pointeY = null, couleur = null){
        scale = document.getElementById("img_plan").offsetWidth / 1489;
        pointeur =  SVG.find('.pointeur_'+id_pointeur)[0];
        if(posX){

            posX1 = posX + (25*scale);
            posX2 = posX + (40*scale);
            pointeur.plot([[posX1,pointeur.array()[0][1]], [posX2,pointeur.array()[1][1]], [pointeur.array()[2][0],pointeur.array()[2][1]]]);
        }

        if(posY){

            posY = posY + (20 * scale);
            pointeur.plot([[pointeur.array()[0][0],posY], [pointeur.array()[1][0],posY], [pointeur.array()[2][0],pointeur.array()[2][1]]]);
        }

        if(pos_pointeX){
            point =  SVG.find('.path_'+id_pointeur)[0];
            point.cx(pos_pointeX);
            pointeur.plot([[pointeur.array()[0][0],pointeur.array()[0][1]], [pointeur.array()[1][0],pointeur.array()[1][1]], [point.cx(),pointeur.array()[2][1]]]);
    
        }

        if(pos_pointeY){
            point =  SVG.find('.path_'+id_pointeur)[0];
            pointeur.plot([[pointeur.array()[0][0],pointeur.array()[0][1]], [pointeur.array()[1][0],pointeur.array()[1][1]], [pointeur.array()[2][0],point.cy()]]);
            point.cy(pos_pointeY);
        }

    }


function dragElement(elmnt, pointeur) {
    let id;
    let scale = document.getElementById("img_plan").offsetWidth / 1489;
    if(pointeur == false){
        id = elmnt.id.split("_")[1];
        elmnt.getElementsByClassName("couleurP")[0].onmousedown = dragMouseDown;
    }
    else{
        id = elmnt.classes()[0].split("_")[1];
        elmnt.on("mousedown", dragMouseDown);
        pointeur = true;
    }

  function dragMouseDown(e) {

    e = e || window.event;
    e.preventDefault();
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    posx_finale = pos1;
    posy_finale = pos2;
    posx_finale_pointe = pos1;
    posy_finale_pointe = pos2;
    if(pointeur == false){
    posx_finale = elmnt.offsetLeft - pos1;
    posy_finale = elmnt.offsetTop - pos2;
    
    if(posy_finale>0){
            elmnt.style.top = (posy_finale) + "px";
    }
    if(posx_finale>0){
        elmnt.style.left = (posx_finale) + "px";
    }
    modifPointeur(id,posx_finale , posy_finale);
    document.getElementById("posx-"+id).value = posx_finale/scale;
    document.getElementById("posy-"+id).value = posy_finale/scale;
    }
    else{
        posx_finale_pointe = elmnt.cx() - pos1;
        posy_finale_pointe = elmnt.cy() - pos2;
        modifPointeur(id,null, null, posx_finale_pointe, posy_finale_pointe); 
        document.getElementById("posx_pointe_"+id).value = posx_finale_pointe/scale;
        document.getElementById("posy_pointe_"+id).value = posy_finale_pointe/scale;
    }
    

  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
    document.forms["form_pastille_"+id].submit();
    
  }
}

    function printDiv(){
        let fic = document.getElementById("printable");
        html2canvas(fic,{
            y:fic.offsetTop,
            x:fic.offsetLeft,
            height:fic.offsetHeight+200,
            width:fic.offsetWidth+100
        }).then(function(canvas) {
            canvas.id = "canva";
            document.body.appendChild(canvas);
            let image = document.createElement("img");
            image.src = canvas.toDataURL('image/jpeg', 1.0);
            image.id = "img_print";
            document.body.appendChild(image);
            $("#img_print").print();
            document.body.removeChild(image);
        });
    }

    function pastilleToForm(id, nom, numero, dimension, posX, posY, pos_pointe_x, pos_pointe_y, denom_legende, init_legende,couleur_fond, couleur_police, texte, type_pose, krelief, braille, image_pose, commentaire, ral, vinyle, zone, angle, poly, produit, dico_sous_p, valider, precedent_type_pose,precedent_nom, precedent_numero,precedent_krelief, precedent_braille, precedent_image_pose,precedent_commentaire, precedent_vinyle,precedent_ral, precedent_feuille_poly,precedent_zone,precedent_position_x,precedent_position_y,precedent_position_pointe_x,precedent_position_pointe_y,precedent_angle, precedent_dimension){
        let div_sous_p = document.getElementById("sous_p");
        while(div_sous_p.firstChild){
            div_sous_p.removeChild(div_sous_p.firstChild);
            document.getElementById("num_sous_p").value = 0;
        }
        if(valider == "False"){
            document.getElementById("remise").disabled = false;
        }
        else {
            document.getElementById("remise").disabled = true;
        }
        document.getElementById("suppr_pastille").disabled = false;
        document.getElementById("sub_form_p").disabled = false;
        document.getElementById("id_pastille").value = id;
        document.getElementById("nom_form").value = nom;
        document.getElementById("numero").value = numero;
        document.getElementById("position_x").value = posX;
        document.getElementById("position_y").value = posY;
        document.getElementById("position_pointe_x").value = pos_pointe_x;
        document.getElementById("position_pointe_y").value = pos_pointe_y;
        document.getElementById("couleur_fond").value = couleur_fond;
        document.getElementById("couleur_lettre").value = couleur_police;
        document.getElementById("init_legende").value = init_legende;
        document.getElementById("type_pose").value = type_pose;
        document.getElementById("texte").value = texte;
        document.getElementById("krelief").value = krelief;
        document.getElementById("braille").value = braille;
        document.getElementById("poly").value = poly;
        document.getElementById("commentaire").value = commentaire;
        document.getElementById("zone").value = zone;
        document.getElementById("rangeAngle").value = angle;
        document.getElementById("valeur_rotation").innerText = angle + "°";
        document.getElementById("nom_produit").value = produit;
        document.getElementById("dimension").value = dimension;
        if(image_pose != "" && image_pose != "None"){
            let img = document.createElement('img');
            img.src = "/static/img/"+image_pose;
            document.getElementById("div_img_pose").appendChild(img);
        }
        if(vinyle == "False" || vinyle == "None"){
            document.getElementById("check_vinyle").checked = false;
        }
        else{
            document.getElementById("check_vinyle").checked = true; 
        }
        if(ral != "None"){
            document.getElementById("check_ral").checked = true;
            document.getElementById("ral").value = ral;
        }
        else{
            document.getElementById("check_ral").checked = false;
        }
        afficheRal();
        let range = document.getElementById("rangeAngle")
        
        range.onfocus = function(){
            rotationPointeur(id,range.value);
        };
        let i = 0;
        for(elem in dico_sous_p){
            ajoutSousProduit(id);
            document.getElementById("sous_produit_" +i).value = elem;
            i++;
        }
        
        // input remise
        document.getElementById("id_pastille_remise").value = id;
        //input suppr
        document.getElementById("id_pastille_supprimer").value = id;
        console.log(document.getElementById("id_pastille_supprimer"));
        if(numero != precedent_numero && precedent_numero != ""){
            document.getElementById("numero").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("numero").style.background = "white";
        }
        if(nom != precedent_nom && precedent_numero != ""){
            document.getElementById("nom_form").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("nom_form").style.background = "white";
        }
        if(type_pose != precedent_type_pose && precedent_numero != ""){
            document.getElementById("type_pose").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("type_pose").style.background = "white";
        }
        if(krelief != precedent_krelief && precedent_numero != ""){
            document.getElementById("krelief").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("krelief").style.background = "white";
        }
        if(braille != precedent_braille && precedent_numero != ""){
            document.getElementById("braille").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("braille").style.background = "white";
        }
        if(image_pose != precedent_image_pose && precedent_numero != ""){
            document.getElementById("image_pose").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("image_pose").style.background = "white";
        }
        if(commentaire != precedent_commentaire && precedent_numero != ""){
            document.getElementById("commentaire").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("commentaire").style.background = "white";
        }
        if(vinyle != precedent_vinyle && precedent_numero != ""){
            document.getElementById("check_vinyle").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("check_vinyle").style.background = "white";
        }
        if(ral != precedent_ral && precedent_numero != ""){
            document.getElementById("ral").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("ral").style.background = "white";
        }
        if(poly != precedent_feuille_poly && precedent_numero != ""){
            document.getElementById("poly").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("poly").style.background = "white";
        }
        if(zone != precedent_zone && precedent_numero != ""){
            document.getElementById("zone").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("zone").style.background = "white";
        }
        if(posX != precedent_position_x && precedent_numero != ""){
            document.getElementById("position_x").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("position_x").style.background = "white";
        }
        if(posY != precedent_position_y && precedent_numero != ""){
            document.getElementById("position_y").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("position_y").style.background = "white";
        }
        if(pos_pointe_x != precedent_position_pointe_x && precedent_numero != ""){
            document.getElementById("position_pointe_x").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("position_pointe_x").style.background = "white";
        }
        if(pos_pointe_y != precedent_position_pointe_y && precedent_numero != ""){
            document.getElementById("position_pointe_y").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("position_pointe_y").style.background = "white";
        }
        if(angle != precedent_angle && precedent_numero != ""){
            document.getElementById("rangeAngle").style.background = "lightgoldenrodyellow";
        }
        else{
            document.getElementById("rangeAngle").style.background = "white";
        }
        if(dimension != precedent_dimension && precedent_numero != ""){
            document.getElementById("dimension").style.background = "lightgoldenrodyellow";
            
        }
        else{
            
            document.getElementById("dimension").style.background = "white";
        }
        console.log(dimension);
        console.log(precedent_dimension);
    }

    function gererInfos(id,icon_id){
        let icon = document.getElementById(icon_id);
        let fieldset = document.getElementById(id);""
        
        let ral = fieldset.getElementsByClassName("color_ral")[0];
        if(fieldset.style.visibility == "hidden"){
            fieldset.style.visibility = "visible";
            fieldset.style.position = "relative";
            icon.classList.replace("fa-chevron-down","fa-chevron-up");
            if(ral != undefined){
                afficheRal();
            }
        }
        else {
            fieldset.style.visibility = "hidden";
            fieldset.style.position = "absolute";
            if(ral != undefined){
                ral.style.visibility = "hidden";
            }
            icon.classList.replace("fa-chevron-up","fa-chevron-down");
        }
        
    }

    function afficheRal(){
        let div_ral = document.getElementById("color_ral");
        let checkbox = document.getElementById("check_ral");
        if(checkbox.checked == true){
            div_ral.style.visibility = "visible";
            div_ral.style.position = "relative";
        }
        else{
            div_ral.style.visibility = "hidden";
            div_ral.style.position = "absolute";
        }
    }

    function rotationPointeur(id, prec){
        let range = document.getElementById("rangeAngle")
        range.onchange = function(){
            rotation = range.value - prec;
            let path =  SVG.find('.path_'+id)[0];
            path.rotate(rotation);
            document.getElementById("valeur_rotation").innerText = range.value + "°";
            prec=range.value;
        };   
    }

    function afficheLegende(type_prod, init_legende, couleur_fond, couleur_lettre, id, dimension){
        console.log(dimension);
        let div = document.getElementsByClassName("les-infos")[0];
        let ul = div.getElementsByTagName("ul")[0];
        let li = document.createElement("li");
        let span = document.createElement("span");
        let innerdiv = document.createElement("div");
        let divNombre = document.createElement("div");
        innerdiv.classList.add("couleurP");
        innerdiv.style.background = couleur_fond;
        innerdiv.style.color = couleur_lettre;
        innerdiv.style.borderRadius = "50px";
        innerdiv.innerText = init_legende;
        innerdiv.addEventListener("click", function (){
            let inputNouvellePastille = document.getElementById("legende_id");
            inputNouvellePastille.value = id;
            document.forms["form_nouvelle_pastille"].submit();
        });
        span.innerText = type_prod + "\n"+dimension;
        divNombre.style.background = "white"
        divNombre.style.borderRadius = "50px";
        divNombre.id = "divNombre" + id;
        divNombre.classList.add("couleurP");
        divNombre.innerText = "0";
        li.classList.add("elem_legende");
        li.appendChild(innerdiv);
        li.appendChild(span);
        li.appendChild(divNombre);
        ul.appendChild(li);
    }

    function ajoutSousProduit(id_pastille){
        let div = document.getElementById("sous_p");
        let form_groupe = document.createElement("div");
        let input = document.createElement("input");
        let input_qte = document.createElement("input");
        let numero = document.getElementById("num_sous_p");
        let nom = "sous_produit_" + numero.value;
        let label = document.createElement("label");
        let label2 = document.createElement("label");
        let label3 = document.createElement("label");
        let div_label =  document.createElement("div");
        let icon_suppr = document.createElement("i");
        let btn_suppr = document.createElement("button");
        let form = document.createElement("form");
        let input_suppr = document.createElement("input");
        let input_fic = document.createElement("input");
        let input_id_pastille = document.createElement("input");
        input_id_pastille.setAttribute("type", "hidden");
        input_id_pastille.setAttribute("value",  id_pastille);
        input_id_pastille.setAttribute("name", "id_pastille");
        input_fic.setAttribute("type", "hidden");
        input_fic.setAttribute("value",  document.getElementById("fic_id").value);
        input_fic.setAttribute("name", "id_fic");
        input_suppr.setAttribute("type", "hidden");
        input_suppr.setAttribute("value", numero.value );
        input_suppr.setAttribute("name", "num_sous_p");
        form.appendChild(input_suppr);
        form.appendChild(input_id_pastille);
        form.setAttribute("action", "/projet/fichier/commit/suppression_sous_produit");
        form.setAttribute("method", "POST");
        form.appendChild(input_fic);
        form.appendChild(input_suppr);
        form_groupe.appendChild(form);
        btn_suppr.classList = "btn btn-danger";
        btn_suppr.setAttribute("type", "button");
        btn_suppr.addEventListener("click", function (){supprSousProduit("form_suppr_" + Number(numero.value-1))});
        form.id = "form_suppr_" + numero.value;
        icon_suppr.classList = "fa fa-remove";
        btn_suppr.appendChild(icon_suppr);
        text = Number(numero.value) +1;
        label.innerHTML = "<b> Sous-produit " + text + " <b>";
        label.appendChild(btn_suppr);
        label2.innerHTML = "Nom : ";
        label3.innerHTML = "Quantité : "
        form_groupe.classList = "form-group";
        input.setAttribute("list", "produits");
        input.setAttribute("name", nom);
        input.id = nom;
        input.classList = "form-control";

        input_qte.setAttribute("type", "number");
        input_qte.setAttribute("name", nom + "_qte");
        input_qte.id = nom+ "_qte";
        input_qte.classList = "form-control";

        div_label.style.display = "flex";
        div_label.style.flexDirection = "column";
        div_label.appendChild(label);
        div_label.appendChild(label2);
        form_groupe.appendChild(div_label);
        form_groupe.appendChild(input);
        form_groupe.appendChild(label3);
        form_groupe.appendChild(input_qte);
        div.appendChild(form_groupe);
        numero.value = Number(numero.value) + 1;
    }

    function supprSousProduit(id_form){
        document.forms[id_form].submit();
    }

    function zoomer(){
        let input = document.getElementById("sens_zoom");
        input.setAttribute("value", "ZOOM");
        console.log(input.getAttribute("value"))
        document.forms["form_zoom"].submit();

    }

    function dezoomer(){
        let input = document.getElementById("sens_zoom");
        input.setAttribute("value", "DEZOOM");
        document.forms["form_zoom"].submit();
    }

    function adapterImage(valeur_zoom){
        console.log(valeur_zoom);
        let array = document.getElementsByClassName("img_plan");
        array.forEach(element => {
            element.style.width = element.width * valeur_zoom + "px";
        });
        
    }