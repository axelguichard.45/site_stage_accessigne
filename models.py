from tkinter.tix import COLUMN
from flask_login import UserMixin
import requests
import pandas as pd


from .app import db

class Utilisateur(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(60))
    nom = db.Column(db.String(40))
    prenom = db.Column(db.String(40))
    mdp = db.Column(db.String(150))
    role = db.Column(db.String(40))
    mail = db.Column(db.String(100))
    projets = db.relationship("Projet",
                    secondary="participe",
                    backref= "utilisateurs")

class Projet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    intitule = db.Column(db.String(40))
    date_deb = db.Column(db.Date)
    validation_client = db.Column(db.Boolean)
    etat = db.Column(db.String(40))
    fichiers = db.relationship("Fichier", backref="projet_fichier")
    legendes = db.relationship("Legende", backref="projet_legende")


class Produit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type_produit = db.Column(db.String(50))
    modele = db.Column(db.String(250))
    description = db.Column(db.String(250))
    url_image = db.Column(db.String(100))
    dimension = db.Column(db.String(50))
    pastilles = db.relationship("Pastille", backref="produit_pastille")
    legendes = db.relationship("Legende", backref="produit_legende")

    pastilles_sous_produit = db.relationship("AssociationConstitue",
                    back_populates= "sous_produit")

class Legende(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    init_legende = db.Column(db.String(5))
    couleur_fond = db.Column(db.String(10))
    couleur_lettre = db.Column(db.String(10))
    dimension = db.Column(db.String(40))
    id_projet = db.Column(db.Integer, db.ForeignKey('projet.id'))
    pastilles = db.relationship("Pastille", backref="legende_pastille")
    id_produit = db.Column(db.Integer, db.ForeignKey('produit.id'))

class Pastille(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    numero = db.Column(db.Integer)
    type_pose = db.Column(db.String(40))
    nom = db.Column(db.String(50))
    texte = db.Column(db.String(100))
    krelief = db.Column(db.Integer)
    braille = db.Column(db.Integer)
    image_pose = db.Column(db.String(100))
    commentaire = db.Column(db.String(400))
    vinyle = db.Column(db.Boolean)
    ral = db.Column(db.String(10))
    feuille_poly = db.Column(db.Integer)
    zone = db.Column(db.String(50))
    position_x = db.Column(db.Integer)
    position_y = db.Column(db.Integer)
    position_pointe_x = db.Column(db.Integer)
    position_pointe_y = db.Column(db.Integer)
    angle = db.Column(db.Integer)
    valider = db.Column(db.Boolean)
    sauvegarde_modif = db.Column(db.Boolean)
    fichier_id = db.Column(db.Integer, db.ForeignKey('fichier.id'))
    produit_id = db.Column(db.Integer, db.ForeignKey('produit.id'))
    id_legende = db.Column(db.Integer, db.ForeignKey('legende.id'))
    sous_produits = db.relationship("AssociationConstitue",
                    back_populates= "pastille_sous_produit")
Pastille.pastille_modif_id = db.Column(db.Integer, db.ForeignKey('pastille.id'))
Pastille.precedente_pastille = db.relationship("Pastille", backref='pastille_precedente',
    remote_side="Pastille.id")
class Fichier(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    chemin = db.Column(db.String(150))
    projet_id = db.Column(db.Integer, db.ForeignKey('projet.id'))
    zoom = db.Column(db.Integer)
    pastilles = db.relationship("Pastille", backref="fichier_pastille")
    chemin_images = db.relationship("Image", backref="fichier_image")


class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(150))
    fichier_id = db.Column(db.Integer, db.ForeignKey('fichier.id'))

class AssociationConstitue(db.Model):
    pastille_id = db.Column(db.Integer, db.ForeignKey('pastille.id'), primary_key = True)
    sous_produit_id = db.Column(db.Integer, db.ForeignKey('produit.id'), primary_key = True)
    quantite = db.Column(db.Integer)
    pastille_sous_produit = db.relationship("Pastille", back_populates="sous_produits")
    sous_produit = db.relationship("Produit", back_populates="pastilles_sous_produit")



association_participe = db.Table('participe',db.metadata,
                                 db.Column('utilisateur_id', db.Integer,
                                            db.ForeignKey('utilisateur.id')),
                                 db.Column('projet_id', db.Integer,
                                            db.ForeignKey('projet.id'))
                                )

class ORM:
    # ----------------------------------------------> Utilisateur <----------------------------------------------#

    @staticmethod
    def find_by_id(id):
        return Utilisateur.query.filter_by(id=id).first() 

    @staticmethod
    def find_by_login(login):
        return Utilisateur.query.filter(Utilisateur.login == login).first()
    
    @staticmethod
    def get_all_users():
        return Utilisateur.query.all()
    
    @staticmethod
    def get_users_15():
        return Utilisateur.query.limit(15).all()
    
    @staticmethod
    def recherche_user_nom(recherche):
        return Utilisateur.query.filter(Utilisateur.nom.contains(recherche) ).all()

    @staticmethod
    def recherche_user_prenom(recherche):
        return Utilisateur.query.filter(Utilisateur.prenom.contains(recherche) ).all()

    # ----------------------------------------------> Projet <----------------------------------------------#

    @staticmethod
    def get_projet_par_utilisateur(id):
        return Projet.query.filter(Projet.utilisateurs.any(id=id)).order_by(Projet.date_deb.desc()).limit(9).all()
    
    @staticmethod
    def get_all_projets():
        return Projet.query.all()
    
    @staticmethod
    def get_projet_by_id(id):
        return Projet.query.filter_by(id = id).first() 

    # ----------------------------------------------> RECHERCHE <----------------------------------------------#

    @staticmethod
    def recherche_projet(recherche, id):
        return Projet.query.filter(Projet.intitule.contains(recherche),Projet.utilisateurs.any(id=id) ).order_by(Projet.date_deb.desc()).all()

    # ----------------------------------------------> FICHIERS <----------------------------------------------#
    @staticmethod
    def get_all_fic():
        return Fichier.query.all()
    
    @staticmethod
    def get_fic_by_id(id):
        return Fichier.query.filter_by(id = id).first()

    # ----------------------------------------------> PASTILLES <----------------------------------------------#

    @staticmethod
    def get_pastilles_by_fichier(id):
        return Pastille.query.filter_by(fichier_id = id).all()
    
    @staticmethod
    def get_pastille_by_id(id):
        return Pastille.query.filter_by(id = id).first()
    
    @staticmethod
    def get_pastille_all():
        return Pastille.query.all()
    
    @staticmethod
    def get_pastille_by_type_produit(id):
        return Pastille.query.filter_by(produit_id = id).all()
    
    # ----------------------------------------------> IMAGE <----------------------------------------------#
    @staticmethod
    def get_all_images():
        return Image.query.all()

    # ----------------------------------------------> PRODUIT <----------------------------------------------#

    @staticmethod
    def get_all_produits():
        return Produit.query.all()

    @staticmethod
    def get_produits_by_id(id):
        return Produit.query.filter_by(id = id).first()

    @staticmethod
    def get_produits_15():
        return Produit.query.limit(15).all()    
    
    @staticmethod
    def get_produit_by_model(model):
        return Produit.query.filter_by(modele = model).first()
    
    @staticmethod
    def get_nom_produits():
        return [modele for modele, in db.session.query(Produit.modele).all()]
    
    @staticmethod
    def get_id_produits():
        return [id for id, in db.session.query(Produit.id).all()]
    
    @staticmethod
    def recherche_produit(recherche):
        return Produit.query.filter(Produit.modele.contains(recherche)).all()

    # ----------------------------------------------> LEGENDES <----------------------------------------------#

    @staticmethod
    def get_all_legendes():
        return Legende.query.all()

    @staticmethod
    def get_legende_by_id(id):
        return Legende.query.filter_by(id = id).first()

    @staticmethod
    def get_legende_by_couleur(couleur):
        return Legende.query.filter_by(couleur = couleur).first()
    
    @staticmethod
    def get_legende_by_initiales(initiales):
        return Legende.query.filter_by(init_legende = initiales).first()
    
    @staticmethod
    def get_legendes_by_projet(id):
        return Legende.query.filter_by(id_projet = id).all()


    # ----------------------------------------------> CSV <----------------------------------------------#

def parseCSVAdmin(filePath):
    error = list()
    success = list()
    les_id = ORM.get_id_produits()
    les_noms = ORM.get_nom_produits()
    csvData = pd.read_csv(filePath, sep= ";", encoding= "latin1")
    for i, row in csvData.iterrows():
        print(row["Nom du produit"])
        if row["Id du produit"] != None and row["Id du produit"] not in (les_id):
            if row["Nom du produit"] != None and row["Nom du produit"] not in les_noms:
                if row["Description"] != None:
                    produit = Produit(id = row["Id du produit"], modele = row["Nom du produit"], description = row["Description"])
                    db.session.add(produit)
                    
                else:
                    error.append(
                            "Le produit -->" + row['Nom du produit'] + "<-- n'a pas de description")
            else :
                error.append(
                        "Le produit -->" + str(row['Id du produit']) + "<-- a un problème de nom")
        else:
            error.append("Le produit -->" + row['Nom du produit'] + "<-- a un problème d'id")
    db.session.commit()
    if(len(error) == 0):
        success.append("Votre jeu de donnée à bien été inséré sans problème")
    return success, error