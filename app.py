import os.path

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename


def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))


app = Flask(__name__, static_url_path='/static')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SECRET_KEY'] = "de046956-23b4-44fe-a848-34205aeb3274"
app.config['SQLALCHEMY_DATABASE_URI'] =  'sqlite:///' + '/home/Accessigne/site_stage_accessigne/donnees.db'    
       
        
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# Upload folder
UPLOAD_FOLDER = './site_stage_accessigne/static/files'
ALLOWED_EXTENSIONS = {'csv', "pdf", "xlsm" }
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
db = SQLAlchemy(app)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'axelguichard.45@gmail.com'
app.config['MAIL_PASSWORD'] = 'axelipop'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


from site_stage_accessigne.models import ORM
from site_stage_accessigne.auth import auth as auth_blueprint

app.register_blueprint(auth_blueprint)
from site_stage_accessigne.views import views as views_blueprint

app.register_blueprint(views_blueprint)

login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.init_app(app)
login_manager.login_message = u"Veuillez vous connecter pour accéder à cette page."


@login_manager.user_loader
def load_user(id):
    return ORM.find_by_id(id)
