import csv
import io
import os
import sqlite3
import datetime
import flask_mail
from datetime import date, time, timedelta
from pdf2image import convert_from_path, convert_from_bytes

from flask import render_template, redirect, Blueprint, request, flash, Response, url_for
from flask_login import login_required, logout_user, current_user
from flask_mail import Message
from flask_wtf import FlaskForm
from werkzeug.utils import secure_filename
from werkzeug.security import generate_password_hash, check_password_hash
from wtforms import SubmitField
from wtforms.fields.html5 import DateField

from .app import app
from .app import ALLOWED_EXTENSIONS, mail
from .models import *

views = Blueprint('views', __name__)

# --------------------------------------> FONCTIONS POUR LE HOME <--------------------------------------#

@app.route("/" )
@login_required
def home():
    les_projets = ORM.get_projet_par_utilisateur(current_user.id)
    return render_template("home.html",
                            titre = "Vos derniers projets",
                            projets = les_projets
                            )

@app.route("/", methods = ["POST"])
@login_required
def search():
    recherche = request.form.get('recherche')
    les_projets = ORM.recherche_projet(recherche, current_user.id)
    return render_template("home.html",
                            titre = "Vos derniers projets",
                            projets = les_projets
                            )

@app.route("/projet/<int:id>/quit", methods = ["POST", "GET"])
@login_required
def quitter_projet(id):
    projet = ORM.get_projet_by_id(id)
    user = ORM.find_by_id(current_user.id)
    user.projets.remove(projet)
    db.session.commit()
    return redirect(url_for('home'))

# --------------------------------------> FONCTIONS POUR LES PROJETS <--------------------------------------#
@app.route("/projet/<int:id>")
@login_required
def afficher_projet(id):
    projet = ORM.get_projet_by_id(id)
    return render_template("projet.html",
                           titre = "Projet " + projet.intitule,
                           projet = projet
                           )

@app.route("/projet/<int:id>/supprimer_fichier/<int:id_fic>")
@login_required
def supprimer_fic(id, id_fic):
    projet = ORM.get_projet_by_id(id)
    fic = ORM.get_fic_by_id(id_fic)
    projet.fichiers.remove(fic)
    db.session.delete(fic)
    db.session.commit()
    return render_template("projet.html",
                           titre = "Projet " + projet.intitule,
                           projet = projet
                           )

@app.route("/projet/creer/")
@login_required
def nouveau_projet():
    users = ORM.get_all_users()
    liste_identifiant = []
    for user in users:
        liste_identifiant.append(user.login)
    return render_template("nouveau_projet.html",
                            titre = "Nouveau Projet",
                            users = liste_identifiant
                            )

@app.route("/projet/creer/post", methods = ["POST"])
@login_required
def nouveau_projet_post():
    id_proj = ORM.get_all_projets()[-1].id +1
    if not id_proj :
        id_proj = 1
    liste_utilisateurs = []
    utilisateurs = request.form.get("participants")
    utilisateurs = utilisateurs.split(",")
    intitule_proj = request.form.get("intitule")
    date = request.form.get("date_deb")
    annee = int(date.split("-")[0])
    mois = int(date.split("-")[1])
    jour = int(date.split("-")[2])
    date_debut = datetime.date(annee, mois, jour)
    nouveau_projet = Projet(id = id_proj, intitule = intitule_proj, date_deb = date_debut, etat = "en cours")
    db.session.add(nouveau_projet)
    current_user.projets.append(nouveau_projet)
    for utilisateur in utilisateurs:
        u = (ORM.find_by_login(utilisateur))
        u.projets.append(nouveau_projet)
    db.session.commit()
    return redirect(url_for("home"))

@app.route("/projet/valider/<int:id>", methods = ["POST"])
@login_required
def valider_projet(id):
    projet = ORM.get_projet_by_id(id)
    projet.validation_client = True
    projet.etat = "Validé"
    db.session.commit()
    msg = flask_mail.Message("[Accessigne] Compte Accessigne",
                             sender='axelguichard.45@gmail.com', recipients=[current_user.mail])
    msg.body = "Vous avez à ce jour validé la proposition signalétique d'Accessigne.\
 Toutes les réalisations seront conforme à ces plans et servirons de preuve en cas de litige.\
\nCordialement, l'équipe Accessigne"
    mail.send(msg)
    return redirect(url_for("afficher_projet", id=id))


@app.route("/projet/remiser/<int:id>", methods = ["POST"])
@login_required
def remiser_projet(id):
    projet = ORM.get_projet_by_id(id)
    projet.validation_client = False
    projet.etat = "En cours"
    db.session.commit()
    msg = flask_mail.Message("[Accessigne] Compte Accessigne",
                             sender='axelguichard.45@gmail.com', recipients=[current_user.mail])
    msg.body = "A ce jour la validation de la proposition signalétique d'Accessigne a été annulé.\
 Les plans peuvent donc être changer à nouveau.\
\nCordialement, l'équipe Accessigne"
    mail.send(msg)
    return redirect(url_for("afficher_projet", id=id))

# --------------------------------------> FONCTIONS POUR LES UTILISATEURS <--------------------------------------#

@app.route("/utilisateurs/")
@login_required
def afficher_utilisateurs():
    utilisateurs = ORM.get_all_users()
    return render_template("utilisateurs.html",
                            titre = "Gerer les utilisateurs",
                            users = utilisateurs
                            )

@app.route("/utilisateurs/", methods = ["GET","POST"])
@login_required
def recherche_utilisateur():
    recherche = request.form.get('recherche')
    precision = request.form.get("precision")
    if(precision == "nom"):
        utilisateurs = ORM.recherche_user_nom(recherche)
    else:
        utilisateurs = ORM.recherche_user_prenom(recherche)

    return render_template("utilisateurs.html",
                            titre = "Gerer les utilisateurs",
                            users = utilisateurs
                            )

@app.route("/utilisateur/creer/")
@login_required
def nouvel_utilisateur():
    return render_template("nouvel_utilisateur.html",
                            titre = "Nouvel Utilisateur",
                            )

@app.route("/utilisateur/creer/post", methods = ["POST"])
@login_required
def nouvel_utilisateur_post():
    identifiant = request.form.get("identifiant")
    user_test = ORM.find_by_login(identifiant)
    if user_test:
        flash('Ce login est déjà attribué', 'error')
        return redirect(url_for("nouvel_utilisateur"))
    mdp_util = request.form.get("password")
    confirm_mdp = request.form.get("confirmpass")
    if mdp_util != confirm_mdp:
        flash("Erreur de confirmation de mot de passe", 'error')
        return redirect(url_for("nouvel_utilisateur"))
    id_util = ORM.get_all_users()[-1].id +1
    if not id_util:
        id_util = 1
    nom_util = request.form.get("nom")
    prenom_util = request.form.get("prenom")
    identifiant = request.form.get("identifiant")
    role_util = request.form.get("role")
    mail_util = request.form.get("email")
    new_user = Utilisateur(id = id_util, login = identifiant, nom = nom_util, prenom = prenom_util, role = role_util, mail = mail_util, mdp=generate_password_hash(mdp_util, method='sha256'))
    db.session.add(new_user)
    db.session.commit()
    #msg = flask_mail.Message("[Accessigne] Compte Accessigne",
    #                         sender='axelguichard.45@gmail.com', recipients=[mail_util])
    #msg.body = "Vous avez été inscrit sur le site Accessigne.\
#Il vous permet de consulter vos plans ainsi que\
#de les modifier et les valider. \n\
#Votre identifiant : " + identifiant + " \n\
#Votre mot de passe : " + mdp_util + " \n \n\
#Cordialement, l'équipe Accessigne"
#    mail.send(msg)
    flash("L'utilisateur a bien été enregistré", 'success')
    return redirect(url_for("afficher_utilisateurs"))

@app.route("/utilisateur/profile/<int:id>")
@login_required
def affiche_profile(id):
    user = ORM.find_by_id(id)
    projets = ORM.get_projet_par_utilisateur(id)
    return render_template("profile.html",
                           titre = "Profil",
                           user = user,
                           projets = projets
                           )

@app.route("/utilisateur/profile/<int:id>/modif")
@login_required
def modif_profile(id):
    user = ORM.find_by_id(id)
    projets = ORM.get_projet_par_utilisateur(id)
    return render_template("profil_modif.html",
                           titre = "Profil",
                           user = user,
                           projets = projets
                           )

@app.route("/utilisateur/profil/modif/post", methods = ["POST"])
@login_required
def modif_profile_post():
    mdp = request.form.get("password")
    confirmation = request.form.get("confirmpass")
    id = request.form.get("id")
    if mdp != confirmation:
        flash("Erreur de confirmation de mot de passe", 'error')
        return redirect(url_for("modif_profile", id = id))
    user = ORM.find_by_id(id)
    user.nom = request.form.get("nom")
    user.prenom = request.form.get("prenom")
    user.mail = request.form.get("mail")
    user.role = request.form.get("role")
    user.login = request.form.get("login")
    if mdp:
        user.mdp = request.form.get("password")
    db.session.commit()
    flash("Le profil a été modifié.", 'success')
    return redirect(url_for("affiche_profile", id = id))


@app.route("/utilisateur/profil/supprimer", methods = ["POST"])
@login_required
def supression_profile_post():
    id = request.form.get("id")
    user = ORM.find_by_id(id)
    db.session.delete(user)
    db.session.commit()
    flash("le profil a été supprimé.", 'success')
    return redirect(url_for("afficher_utilisateurs"))

# --------------------------------------> FONCTIONS POUR LES PRODUITS <--------------------------------------#


@app.route("/produits/", methods = ["GET"])
@login_required
def afficher_produits():
    produits = ORM.get_produits_15()
    return render_template("produits.html",
                            titre = "Gerer les produits",
                            produits = produits
                            )

@app.route("/produits/", methods = ["GET","POST"])
@login_required
def recherche_produit():
    recherche = request.form.get('recherche')
    produits = ORM.recherche_produit(recherche)
    return render_template("produits.html",
                            titre = "Gerer les produits",
                            produits = produits
                            )

@app.route("/produits/nouveau")
@login_required
def nouveau_produit():
    return render_template("nouveau_produit.html",
                            titre = "Ajouter un produit"
                            )


@app.route("/produits/nouveau/post", methods = ["POST"])
@login_required
def nouveau_produit_post():
    nom = request.form.get("nom")
    descr = request.form.get("description")
    id = ORM.get_all_produits()[-1].id
    new_produit = Produit(id = id+1, modele = nom, description = descr)
    db.session.add(new_produit)
    db.session.commit()
    return redirect(url_for("afficher_produits"))

@app.route("/produits/supprimer", methods = ["POST"])
@login_required
def supprimer_produit():
    id = request.form.get("prod_id")
    produit = ORM.get_produits_by_id(id)
    for asso in produit.pastilles_sous_produit:
        db.session.delete(asso)
    db.session.delete(produit)
    db.session.commit()
    return redirect(url_for("afficher_produits"))

# --------------------------------------> FONCTIONS POUR LES FICHIERS <--------------------------------------#


def allowed_file(filename):
    """
    Valide l'extension d'un fichier

    :param filename: [description]
    :type filename: [type]
    :return: Retourne True si le fichier est valide sinon False
    :rtype: bool
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/projet/upload_file", methods = ["POST", "GET"])
@login_required
def upload_file():
    projet_id = request.form.get("projetID")
    if request.method == 'POST':
        if 'fileToUpload' not in request.files:
            flash('No file part')
            return redirect(url_for("afficher_projet",id= projet_id))
        file = request.files['fileToUpload']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for("afficher_projet",id= projet_id))
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            id_fic = ORM.get_all_fic()[-1].id +1
            if not id_fic:
                id_fic = 1
            images = convert_from_path(os.path.join(app.config['UPLOAD_FOLDER'],filename), poppler_path = "/usr/bin", output_folder = app.config['UPLOAD_FOLDER'],paths_only=True, fmt = "jpeg")
            new_fic = Fichier(id = id_fic, nom = filename, chemin = url_for('static', filename = 'files/'+filename), projet_id = projet_id, zoom = 1)
            db.session.add(new_fic)
            for chemin in images:
                nom = chemin.split("/")[len(chemin.split("/"))-1]
                id_img = ORM.get_all_images()[-1].id +1
                if not id_img:
                    id_img = 1
                image = Image(id = id_img, nom = nom, fichier_id = id_fic)
                db.session.add(image)
            db.session.commit()
            return redirect(url_for("afficher_projet", id = projet_id))

        else:
            flash("Problème d'extension", 'error')
            return redirect(url_for("afficher_projet", id = projet_id))

@app.route("/projet/fichier/<int:id>")
@login_required
def affiche_fichier(id):
    fichier = ORM.get_fic_by_id(id)
    produits = ORM.get_all_produits()
    legendes = ORM.get_legendes_by_projet(fichier.projet_id)
    pastilles = ORM.get_pastilles_by_fichier(fichier.id)
    return render_template("fichier.html",
                     titre = fichier.nom,
                     fichier = fichier,
                     images = fichier.chemin_images,
                     pastilles = pastilles,
                     legendes = legendes,
                     produits = produits
                    )

@app.route("/projet/fichier/commit/pastille_position", methods = ["POST"])
@login_required
def save_pastille_position():
    id_pastille = request.form.get("id_pastille")
    posx = request.form.get("posx")
    posy = request.form.get("posy")
    pos_pointe_x = request.form.get("posx_pointe")
    pos_pointe_y = request.form.get("posy_pointe")
    id_fic = request.form.get("fic")
    pastille = ORM.get_pastille_by_id(id_pastille)
    if not pastille.valider or current_user.role == "Implenteur":
        pastille.position_x = posx
        pastille.position_y = posy
        pastille.position_pointe_x = pos_pointe_x
        pastille.position_pointe_y = pos_pointe_y
    else :
        pastille.valider = current_user.role == "Implenteur"
        id_pastille = ORM.get_pastille_all()[-1].id +1
        if not id_pastille:
            id_pastille = 1
        new_pastille = Pastille(id = id_pastille, numero = pastille.numero, type_pose = pastille.type_pose, nom = pastille.nom, texte = pastille.texte, krelief = pastille.krelief, braille = pastille.braille, image_pose = pastille.image_pose, commentaire = pastille.commentaire, vinyle = pastille.vinyle, ral = pastille.ral, feuille_poly = pastille.feuille_poly, zone = pastille.zone, position_x = posx, position_y = posy, position_pointe_y = pos_pointe_y, position_pointe_x = pos_pointe_x, angle = pastille.angle, valider = False, sauvegarde_modif = False, pastille_modif_id = pastille.id, fichier_id = pastille.fichier_id, produit_id = pastille.produit_id)
        pastille.sauvegarde_modif = True
        pastille.pastille_modif_id = new_pastille.id
        pastille.legende_pastille.pastilles.append(new_pastille)
        pastille = new_pastille
    pastille.valider = current_user.role == "Implenteur"
    db.session.commit()
    return redirect(url_for("affiche_fichier",id = id_fic))

@app.route("/projet/fichier/commit/pastille_modifications", methods = ["POST", "GET"])
@login_required
def save_pastille_modifications():
    id_fic = request.form.get("fic")
    id_pastille = request.form.get("id_pastille")
    nom = request.form.get("nom")
    num = request.form.get("numero")
    pose = request.form.get("type_pose")
    dimension = request.form.get("dimension")
    text = request.form.get("texte")
    krelief = request.form.get("krelief")
    braille = request.form.get("braille")
    if 'image_pose' in request.files:
        flash('Pas de fichier')
        file = request.files['image_pose']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('Pas de fichier')
            filename =""
        else :
           # if file and allowed_file(file.filename):

            filename = file.filename
            #filename = secure_filename(file.filename)
            file.save(os.path.join('./site_stage_accessigne/static/img', file.filename))
    else:
        filename = ""
    #img_pose = request.form.get("image_pose")
    com = request.form.get("commentaire")
    pos_x = request.form.get("position_x")
    pos_y = request.form.get("position_y")
    pos_vise_x = request.form.get("position_pointe_x")
    pos_vise_y = request.form.get("position_pointe_y")
    col_fond = request.form.get("couleur_fond")
    col_police = request.form.get("couleur_lettre")
    nom_prod = request.form.get("nom_produit")
    zone = request.form.get("zone")
    vinyle = request.form.get("vinyle")
    on_ral = request.form.get("on_ral")
    ral = request.form.get("ral")
    angle = request.form.get("angle")
    init_legende = request.form.get("init_legende")
    poly = request.form.get("poly")
    nb_sous_produit = request.form.get("num_sous_p")
    fichier = ORM.get_fic_by_id(id_fic)
    projet = fichier.projet_fichier

    if(vinyle == "on"):
        vinyle = True
    else:
        vinyle = False
    if(on_ral != "on"):
        ral = None
    produit = ORM.get_produit_by_model(nom_prod)
    pastille = ORM.get_pastille_by_id(id_pastille)
    legende = ORM.get_legende_by_id(pastille.legende_pastille.id)
    if pastille.valider and current_user.role == "Implenteur":
        # Gestion de la légende
        if legende.init_legende != init_legende:
            test_legende = ORM.get_legende_by_initiales(init_legende)
            if not test_legende or test_legende.id_projet != projet.id:
                last_legende = ORM.get_all_legendes()[-1]
                last_id = 0
                if last_legende:
                    last_id = last_legende.id
                new_legende = Legende(id = last_id +1, init_legende = init_legende, couleur_fond = col_fond, couleur_lettre = col_police, id_projet = projet.id)
                db.session.add(new_legende)
                pastille.id_legende = new_legende.id
                legende = new_legende
            else:
                pastille.id_legende = test_legende.id
                legende = test_legende

      #  if len(legende.pastilles) == 0:
        #        db.session.delete(legende)
        else:
            if legende.couleur_fond != col_fond:
                legende.couleur_fond = col_fond
            if legende.couleur_lettre != col_police:
                legende.couleur_lettre = col_police
        if pastille.nom != nom:
            nomsPatilles = []
            for fic in projet.fichiers:
                for p in fic.pastilles:
                    nomsPatilles.append(p.nom)
            if(nom not in nomsPatilles):
                pastille.nom = nom
            else:
                flash("Nom de pastille déjà utilisé", 'error')
                return redirect(url_for("affiche_fichier",id = id_fic))
        if pastille.type_pose != pose:
            pastille.type_pose = pose
        if pastille.texte != text:
            pastille.texte = text
        if pastille.krelief != krelief:
            pastille.krelief = krelief
        if pastille.braille != braille:
            pastille.braille = braille
        if pastille.image_pose != filename:
            pastille.image_pose = filename
        if pastille.commentaire != com:
            pastille.commentaire = com
        if legende.dimension != dimension:
            legende.dimension = dimension
        if pastille.position_x != pos_x:
            pastille.position_x = pos_x
        if pastille.position_y != pos_y:
            pastille.position_y = pos_y
        if pastille.position_pointe_x != pos_vise_x:
            pastille.position_pointe_x = pos_vise_x
        if pastille.position_pointe_y != pos_vise_y:
            pastille.position_pointe_y = pos_vise_y
        if produit and legende.id_produit != produit.id:
            legende.id_produit = produit.id
            for p in legende.pastilles:
                p.produit_id = legende.id_produit
        if pastille.vinyle != vinyle:
            pastille.vinyle = vinyle
        if pastille.ral != ral:
            pastille.ral = ral
        if pastille.zone != zone:
            pastille.zone = zone
        if pastille.angle != angle:
            pastille.angle = angle
        if pastille.feuille_poly != poly:
            pastille.feuille_poly = poly
    else:
        print("-----------------here------------------")
        id_pastille = ORM.get_pastille_all()[-1].id +1
        if not id_pastille:
            id_pastille = 1
        new_pastille = Pastille(id = id_pastille, numero = num, type_pose = pose, nom = nom, texte = text, krelief = krelief, braille = braille, image_pose = filename, commentaire = com, vinyle = vinyle, ral = ral, feuille_poly = poly, zone = zone, position_x = pos_x, position_y = pos_y, position_pointe_y = pos_vise_y, position_pointe_x = pos_vise_x, angle = angle, valider = False, sauvegarde_modif = False, pastille_modif_id = pastille.id, fichier_id = pastille.fichier_id, produit_id = produit.id)
        pastille.sauvegarde_modif = True
        pastille.pastille_modif_id = new_pastille.id
        legende.pastilles.append(new_pastille)
        pastille = new_pastille

    liste_nom_sous_p = []
    for sous_p in pastille.sous_produits:
        liste_nom_sous_p.append(sous_p.sous_produit.modele)
    for i in range(int(nb_sous_produit)):
        nom = request.form.get("sous_produit_"+str(i))
        sous_produit_qte = request.form.get("sous_produit_"+str(i)+"_qte")
        prod = ORM.get_produit_by_model(nom)
        if(prod):
            if i < len(liste_nom_sous_p) and len(liste_nom_sous_p) > 0:
                if pastille.sous_produits[i].sous_produit.modele!= nom:
                    liste_nom_sous_p.remove(pastille.sous_produits[i].sous_produit.modele)
                    pastille.sous_produits[i].id_sous_produit = prod.id
                    pastille.sous_produits[i].sous_produit = prod
                    liste_nom_sous_p.append(nom)
                if pastille.sous_produits[i].quantite != sous_produit_qte:
                    pastille.sous_produits[i].quantite = sous_produit_qte
            else:
                if(nom not in liste_nom_sous_p):
                    asso = AssociationConstitue(pastille_id = pastille.id, sous_produit_id = prod.id,quantite = sous_produit_qte)
                    asso.sous_produit = prod
                    pastille.sous_produits.append(asso)
                    liste_nom_sous_p.append(nom)
    pastille.valider = current_user.role == "Implenteur"
    db.session.commit()
    return redirect(url_for("affiche_fichier",id = id_fic))

@app.route("/projet/fichier/commit/nouvelle_pastille", methods = ["POST"])
@login_required
def nouvelle_pastille():
    id_fic = request.form.get("fic")
    fic = ORM.get_fic_by_id(id_fic)
    if len(ORM.get_pastille_all()) >0:
        id_past = ORM.get_pastille_all()[-1].id +1
    else :
        id_past = 1
    prev_pastille = ORM.get_pastille_by_id(id_past-1)
    validation = current_user.role == "Implenteur"
    id_legende = request.form.get("legende_id")
    if prev_pastille:
        last_digit = int(prev_pastille.nom[len(prev_pastille.nom)-1]) +1
        nom = prev_pastille.nom[0:len(prev_pastille.nom)-1]
        nom += str(last_digit)
    else :
        nom = 1
    legendes = ORM.get_legendes_by_projet(fic.projet_id)
    if len(legendes) == 0:
        if len(ORM.get_all_legendes())>0:
            id_new = ORM.get_all_legendes()[-1].id +1
        else :
            id_new = 1
        new_legende = Legende(id = id_new,
                       init_legende = "T",
                       couleur_fond = "#800000",
                       couleur_lettre = "#FFFFFF",
                       id_projet = fic.projet_id,
                       id_produit = 1,
                       dimension = "200 x 300"
                       )
        db.session.add(new_legende)
    elif id_legende != '0':
        new_legende = ORM.get_legende_by_id(id_legende)
    elif prev_pastille:
        new_legende = ORM.get_legende_by_id(prev_pastille.id_legende)
    else :
        new_legende = Legende(id = legendes[-1].id + 1,
                       init_legende = "T",
                       couleur_fond = "#800000",
                       couleur_lettre = "#FFFFFF",
                       id_projet = fic.projet_id,
                       id_produit = 1,
                       dimension = "200 x 300"
                       )
    print(new_legende)
    if prev_pastille:
        new_pastille = Pastille(id = id_past,
                                numero = prev_pastille.numero+1,
                                type_pose = prev_pastille.type_pose,
                                nom = nom,
                                krelief = prev_pastille.krelief,
                                braille = prev_pastille.braille,
                                zone = prev_pastille.zone,
                                commentaire = prev_pastille.commentaire,
                                position_x = 300,
                                position_y = 300,
                                position_pointe_x = 400,
                                position_pointe_y = 400,
                                angle = 0,
                                id_legende = new_legende.id,
                                fichier_id = id_fic,
                                produit_id = new_legende.id_produit,
                                valider = validation,
                                feuille_poly =0
                                )
    else :
        Pastille(id = id_past,
                 numero = 1
                                )

    db.session.add(new_pastille)
    db.session.commit()
    return redirect(url_for("affiche_fichier",id = id_fic))


@app.route("/projet/fichier/commit/remise_pastille", methods = ["GET","POST"])
@login_required
def remiser_pastille():
    id_pastille = request.form.get("id_pastille_remise")
    pastille = ORM.get_pastille_by_id(id_pastille)
    prev_pastille = ORM.get_pastille_by_id(pastille.pastille_modif_id)
    if prev_pastille:
        for asso in pastille.sous_produits:
            db.session.delete(asso)
        db.session.delete(pastille)
        prev_pastille.sauvegarde_modif = False
        prev_pastille.valider = True
        db.session.commit()
        flash('Remise effectué', 'success')
    else:
        flash('Pas de remise possible', 'error')
    return redirect(url_for("affiche_fichier",id = pastille.fichier_id))


@app.route("/projet/fichier/zoomer", methods = ["GET","POST"])
@login_required
def zoomer():
    fic = request.form.get("fic")
    sens_zoom = request.form.get("sens_zoom")
    fichier = ORM.get_fic_by_id(fic)
    if(sens_zoom == "ZOOM"):
        valeur_zoom = 0.5
    else :
        valeur_zoom = -0.5
    fichier.zoom += valeur_zoom
    db.session.commit()
    return redirect(url_for("affiche_fichier",id = fic))

@app.route("/projet/fichier/commit/supprimer_pastille", methods = ["GET","POST"])
@login_required
def supprimer_pastille():
    id_pastille = request.form.get("id_pastille_supprimer")
    print(id_pastille)
    pastille = ORM.get_pastille_by_id(id_pastille)
    fic_id = pastille.fichier_id
    for asso in pastille.sous_produits:
        db.session.delete(asso)
    db.session.delete(pastille)
    db.session.commit()
    return redirect(url_for("affiche_fichier",id = fic_id))

@app.route("/projet/fichier/commit/suppression_sous_produit", methods = ["POST"])
@login_required
def supprimer_sous_produit():
    fic_id = request.form.get("id_fic")
    id_pastille = request.form.get("id_pastille")
    pastille = ORM.get_pastille_by_id(id_pastille)
    association = pastille.sous_produits[int(request.form.get("num_sous_p"))]
    db.session.delete(association)
    db.session.commit()
    return redirect(url_for("affiche_fichier",id = int(fic_id)))



# --------------------------------------> FONCTIONS POUR LES CSV <--------------------------------------#


@app.route('/create_csv/<int:id>')
@login_required
def create_csv(id):
    """
    Permet de les pastilles d'un projet en csv

    :param id: ID du projet
    :type pseudo:
    """
    projet = ORM.get_projet_by_id(id)
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    db_path = os.path.join(BASE_DIR, "donnees.db")
    con = sqlite3.connect(db_path)
    data = io.StringIO()

    w = csv.writer(data)

    cursor = con.execute(
        "SELECT  DISTINCT pa.numero, pa.type_pose, pa.nom, pa.texte, pa.krelief, pa.braille, pa.commentaire, pa.vinyle, pa.feuille_poly, pa.produit_id, pa.id,pa.zone, pa.fichier_id, f.nom FROM projet p, fichier f, pastille pa, association_constitue asso WHERE p.id = ? and f.projet_id = p.id and f.id = pa.fichier_id order by fichier_id and zone",
        [id])
    descr = ["Nombre d'élément","Fichier","Zone"]
    for x in cursor.description:
        descr.append(x[0])
    descr = descr[0:len(descr)-5]
    max_sous_p = 0
    descr.append("produit")
    nb_elem = 0
    for elem in cursor:
        nb_elem+=1
        id_pastille = elem[len(elem)-4]
        pastille = ORM.get_pastille_by_id(id_pastille)
        tot_sous_p = 0
        for asso in pastille.sous_produits:
            tot_sous_p += 1
            if tot_sous_p > max_sous_p:
                max_sous_p = tot_sous_p
                descr.append("sous produit " + str(tot_sous_p))
                descr.append("quantite sous produit " + str(tot_sous_p))
    w.writerow(descr)

    #gestions du contenu
    zone_prec = ""
    nom_prec = ""
    cursor = con.execute(
        "SELECT  DISTINCT pa.numero, pa.type_pose, pa.nom, pa.texte, pa.krelief, pa.braille, pa.commentaire, pa.vinyle, pa.feuille_poly,pa.produit_id, pa.id,pa.zone, pa.fichier_id, f.nom FROM projet p, fichier f, pastille pa, association_constitue asso WHERE p.id = ? and f.projet_id = p.id and f.id = pa.fichier_id order by fichier_id and zone",
        [id])
    ligne = 0
    for elem in cursor:
        liste_info = ["","",""]

        for info in elem:
            liste_info.append(info)

        nom_fic = liste_info[len(liste_info)-1]
        liste_info.pop()
        id_fic_pastille = liste_info[len(liste_info)-1]
        liste_info.pop()
        zone = liste_info[len(liste_info)-1]
        liste_info.pop()
        id_pastille = liste_info[len(liste_info)-1]
        liste_info.pop()
        id_produit = liste_info[len(liste_info)-1]
        liste_info.pop()
        produit = ORM.get_produits_by_id(id_produit)
        liste_info.append(produit.modele)
        if ligne == 0:
            ligne += 1
            w.writerow([nb_elem])
        # code pour afficher les sous produits
        pastille = ORM.get_pastille_by_id(id_pastille)
        for asso in pastille.sous_produits:
            liste_info.append(asso.sous_produit.modele)
            liste_info.append(asso.quantite)

        if nom_fic != nom_prec:
            nom_prec = nom_fic
            w.writerow(["",nom_prec])

        if zone != zone_prec:
            zone_prec = zone
            w.writerow(["", "",zone_prec])

        w.writerow(liste_info)
    data.seek(0)
    return Response(data, mimetype="text/csv",
                    headers={"Content-Disposition": "attachment;filename=matrice_projet_%s.csv" % (projet.intitule)})

    #return redirect(url_for("affiche_fichier",id = id))

@app.route("/produits/import/", methods=['POST', 'GET'])
@login_required
def import_csv():
    """
    Permet à un admin d'importer un ensemble de produit dans la base de données
    """
    if 'file' not in request.files:
        flash('Erreur de selection du fichier', 'error')
        return redirect(url_for("afficher_produits"))
    file = request.files['file']
    if file.filename == '':
        flash('Pas de fichier selectionné', 'error')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        successes, errors = parseCSVAdmin(os.path.abspath(
            "site_stage_accessigne/static/files/" + filename))

        for success in successes:
            flash(success, 'success')
        for error in errors:
            flash(error, 'error')
        return redirect(url_for('afficher_produits'))