import datetime
import os

from werkzeug.security import generate_password_hash
from .app import app, db, mkpath
from .models import Utilisateur, Produit, Projet, Pastille, Fichier, Image, Legende, AssociationConstitue
@app.cli.command()
def syncdb():
    '''Creer toutes les tables manquantes'''
    # creation de toutes les tables
    db.create_all()


@app.cli.command()
def loaddb():
    '''Ajoute les éléments à la bd '''
    if os.path.exists(mkpath("./donnees.db")):
        os.remove(mkpath("./donnees.db"))
    else:
        print("Fichier pas là")
    db.create_all()

    # --------------------------> Utilisateur <--------------------------#

    utilisateur1 = Utilisateur(id = 1,
                               login = "axel.guichard",
                               nom = "Guichard",
                               prenom = "Axel",
                               mdp = generate_password_hash("axel", method='sha256'),
                               role = "Implenteur",
                               mail = "sandeku.4545@gmail.com"
                               )
    
    utilisateur2 = Utilisateur(id = 2,
                               login = "paul.pogba",
                               nom = "Pogba",
                               prenom = "Paul",
                               mdp = generate_password_hash("paul", method='sha256'),
                               role = "Architect",
                               mail = "sandeku.4545@gmail.com"
                               )

    utilisateur3 = Utilisateur(id = 3,
                               login = "maire.troyes",
                               nom = "Maire",
                               prenom = "Troyes",
                               mdp = generate_password_hash("maire", method='sha256'),
                               role = "Client",
                               mail = "sandeku.4545@gmail.com"
                               )

    utilisateur4 = Utilisateur(id = 4,
                               login = "valerie.damido",
                               nom = "Damido",
                               prenom = "Valerie",
                               mdp = generate_password_hash("valerie", method='sha256'),
                               role = "Architect",
                               mail = "sandeku.4545@gmail.com"
                               )
    
    db.session.add(utilisateur1)
    db.session.add(utilisateur2)
    db.session.add(utilisateur3)
    db.session.add(utilisateur4)


    # --------------------------> Projet <--------------------------#

    projet1 = Projet(id = 1,
                     intitule = "Vitrines de Troyes",
                     date_deb = datetime.date(2021, 3, 9),
                     etat = "en cours",
                    )

    projet2 = Projet(id = 2,
                     intitule = "Musées de Troyes",
                     date_deb = datetime.date(2021, 3, 1),
                     etat = "en cours"
                    )

    projet3 = Projet(id = 3,
                     intitule = "Piscine de Troyes",
                     date_deb = datetime.date(2020, 3, 1),
                     etat = "terminé"
                    ) 

    projet4 = Projet(id = 4,
                     intitule = "Université",
                     date_deb = datetime.date(2020, 4, 1),
                     etat = "en cours"
                    )
    
    projet5 = Projet(id = 5,
                     intitule = "Avocats",
                     date_deb = datetime.date(2020, 5, 1),
                     etat = "en cours"
                    )
    
    db.session.add(projet1)
    db.session.add(projet2)
    db.session.add(projet3)
    db.session.add(projet4)
    db.session.add(projet5)

    # --------------------------> Produit <--------------------------#

    produit1 = Produit(id = 1,
                       type_produit = "NC",
                       modele = "(BDI) Balise directionnelle imposte : Panneaux Dibond finition Brossé",
                       url_image = "img/num_chambre",
                       description = "Panneaux DIBOND finition brossé1500mn x 216mn avec ajout de lettrage adhésive NoireMontage par collage",
                       )

    produit2 = Produit(id = 2,
                       type_produit = "T",
                       modele = "amer 710 alu",
                       url_image = "img/TOTEM",
                       description = "Totem structure inox 701 x 989 x 75 mm finition alu ano naturel",
                       )

    db.session.add(produit1)
    db.session.add(produit2)

    # --------------------------> Pastille <--------------------------#

    pastille1 = Pastille(id = 1,
                         numero = 1,
                         type_pose = "Visses",
                         nom = "NC-001",
                         texte = "1",
                         krelief = "100",
                         braille = "100",
                         position_x = 200,
                         position_y = 200,
                         position_pointe_x = 200,
                         position_pointe_y = 400,
                         angle = 0,
                         zone = "etage1",
                         feuille_poly = 0,
                         valider = True
                        )

    pastille2 = Pastille(id = 2,
                         numero = 2,
                         type_pose = "Colle",
                         nom = "T-001",
                         texte = "2",
                         krelief = "101",
                         braille = "101",
                         position_x = 300,
                         position_y = 200,
                         position_pointe_x = 400,
                         position_pointe_y = 250,
                         angle = 0,  
                         zone = "etage1",
                         feuille_poly = 0,
                         valider = True           
                        )

    pastille3 = Pastille(id = 3,
                         numero = 1,
                         type_pose = "Colle",
                         nom = "NC-101",
                         texte = "2",
                         krelief = "101",
                         braille = "101",
                         position_x = 250,
                         position_y = 200,
                         position_pointe_x = 400,
                         position_pointe_y = 250,
                         angle = 0,  
                         zone = "etage2",
                         feuille_poly = 0,
                         valider = True                
                        )
    
    pastille4 = Pastille(id = 4,
                         numero = 2,
                         type_pose = "Colle",
                         nom = "T-101",
                         texte = "2",
                         krelief = "101",
                         braille = "101",
                         position_x = 300,
                         position_y = 200,
                         position_pointe_x = 400,
                         position_pointe_y = 250,
                         angle = 0,
                         zone = "etage3",
                         feuille_poly = 0,
                         valider = True
                        )
    

    db.session.add(pastille1)
    db.session.add(pastille2)
    db.session.add(pastille3)
    db.session.add(pastille4)

    # --------------------------> Fichier <--------------------------#

    fichier1 = Fichier(id = 1,
                       nom = "A3 1er Fra-Fer-Sar",
                       chemin = "/static/files/A3 1er Fra-Fer-Sar.pdf",
                       zoom = 1
                       )
    
    fichier2 = Fichier(id = 2,
                       nom = "A3 1er Tri-Sar",
                       chemin = "/static/files/A3 1er Tri-Sar.pdf",
                       zoom = 1
                       )
    
    fichier3 = Fichier(id = 3,
                       nom = "Fic4",
                       chemin = "/static/files/Fic4.pdf",
                       zoom = 1
                       )
    fichier4 = Fichier(id = 4,
                       nom = "fic5",
                       chemin = "/static/files/fic5.pdf",
                       zoom = 1
                       )

    db.session.add(fichier1)
    db.session.add(fichier2)
    db.session.add(fichier3)
    db.session.add(fichier4)
    
    # --------------------------> Image <--------------------------#

    image1 = Image(id = 1,
                   nom = "d4ab4178-4697-4d97-8658-f5f3db0b4d7f-1.jpg",
                   fichier_id = 1
                   )

    image2 = Image(id = 2,
                   nom = "2531911a-495f-493d-b5f3-7a118bcfa2af-1.jpg",
                   fichier_id = 2 
                   )               
    
    db.session.add(image1)
    db.session.add(image2)

    # --------------------------> Legende <--------------------------#
    legende1 = Legende(id = 1,
                       init_legende = "NC",
                       couleur_fond = "#008000",
                       couleur_lettre = "#FFFFFF",                     
                       id_projet = projet1.id,
                       id_produit = produit1.id,
                       dimension = "100 x 200"
                       )
    
    legende2 = Legende(id = 2,
                       init_legende = "T",
                       couleur_fond = "#800000",
                       couleur_lettre = "#FFFFFF",
                       id_projet = projet1.id,
                       id_produit = produit2.id,
                       dimension = "200 x 300"
                       )
    db.session.add(legende1)
    db.session.add(legende2)

    
    # --------------------------> AJOUT DES RELATIONS <--------------------------#
     # ------ Produit Pastille -----#
     
    pastille1.produit_id = produit1.id
    pastille2.produit_id = produit2.id
    pastille3.produit_id = produit1.id
    pastille4.produit_id = produit2.id

    # ------ Legende Pastille -----#
     
    pastille1.id_legende = legende1.id
    pastille2.id_legende = legende2.id
    pastille3.id_legende = legende1.id
    pastille4.id_legende = legende2.id

     # ------ Pastille Fichier -----#

    pastille1.fichier_id = fichier1.id
    pastille2.fichier_id = fichier1.id
    pastille3.fichier_id = fichier2.id
    pastille4.fichier_id = fichier2.id

     # ------ Projet Fichier -----#

    fichier1.projet_id = projet1.id
    fichier2.projet_id = projet1.id
    fichier3.projet_id = projet2.id
    fichier4.projet_id = projet2.id

     # ------ participants projet -----#

    utilisateur1.projets.append(projet1)
    utilisateur1.projets.append(projet2)
    utilisateur1.projets.append(projet3)
    utilisateur1.projets.append(projet4)
    utilisateur1.projets.append(projet5)
    utilisateur2.projets.append(projet1)
    utilisateur3.projets.append(projet1)
    utilisateur3.projets.append(projet2)
    utilisateur4.projets.append(projet2)
     # ------ pastilles sous-produits -----#

    asso1 = AssociationConstitue(pastille_id = pastille1.id, sous_produit_id = produit2.id , quantite = 2)
    asso1.sous_produit = produit1
    asso3 = AssociationConstitue(pastille_id = pastille1.id, sous_produit_id = produit1.id , quantite = 1)
    asso3.sous_produit = produit2
    pastille1.sous_produits.append(asso3)
    pastille1.sous_produits.append(asso1)
    asso2 = AssociationConstitue(pastille_id = pastille2.id, sous_produit_id = produit1.id, quantite = 1)
    pastille2.sous_produits.append(asso2)
    db.session.commit()

