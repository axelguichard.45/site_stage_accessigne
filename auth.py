import datetime
import flask_mail

from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

from .app import app
from .app import mail
from .models import *

auth = Blueprint('auth', __name__)


@auth.route("/login/")
def login():
    return render_template(
        "login.html",
        title="Login"
    )


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("home"))


@auth.route("/login/", methods=['POST'])
def login_post():
    login = request.form.get('login')
    password = request.form.get('password')
    user = ORM.find_by_login(login)
    if not user or not check_password_hash(user.mdp, password):
        flash('Vérifiez vos identifiants et réessayez', 'error')
        return redirect(url_for('auth.login'))
    login_user(user)
    return redirect(url_for("home"))

